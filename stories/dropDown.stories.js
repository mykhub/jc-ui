import React from 'react';
import { storiesOf } from '@storybook/react';
import Emojify from 'react-emojione';

import { DropDown, Badge } from '../src';

const EMOJI_SIZE = {
  width: '18px',
  height: '18px,'
}

storiesOf('DropDown', module)
  .add('default', () => (
    <DropDown>Lorem Ipsum</DropDown>
  ))
  .add('with rotate', () => (
    <DropDown canRotate={true} placeholder='Mailboxes'>
      <DropDown.Item><Emojify style={EMOJI_SIZE}>👪</Emojify><span style={{marginLeft: '3%'}}>Dream team</span></DropDown.Item>
      <DropDown.Item><Emojify style={EMOJI_SIZE}>👔</Emojify><span style={{marginLeft: '3%'}}>Work</span><Badge styles={{right: 0}}>+5</Badge></DropDown.Item>
      <DropDown.Item><Emojify style={EMOJI_SIZE}>📰</Emojify><span style={{marginLeft: '3%'}}>Newsletters</span><Badge styles={{right: 0}}>+2</Badge></DropDown.Item>
    </DropDown>
  ))

