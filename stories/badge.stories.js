import React from 'react';

import { storiesOf } from '@storybook/react';
import { Badge } from '../src';

storiesOf('Badge', module)
  .add('default', () => (
    <Badge>{'+5'}</Badge>
  ));
