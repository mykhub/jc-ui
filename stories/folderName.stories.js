import React from 'react';
import { storiesOf } from '@storybook/react';

import { FolderName } from '../src';

storiesOf('FolderName', module)
  .add('default', () => (
    <FolderName name="Team Peaches" emoji="🍑" />
  ))
  .add('All threads', () => (
    <FolderName name="All threads" emoji="📘" />
  ));

