import React from 'react';

import { storiesOf } from '@storybook/react';
import { Card, AvatarList, Round, TimeAgo, THREAD_TEXT, Badge } from '../src';
import { AVATARS } from '../src/common/constants';
import { ROW, COL, THREAD_HEADER } from '../src';   

const DEFAULT_SOURCES = [
    'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
];
storiesOf('ThreadCard', module)
  .add('group', () => (
    <Card className={COL()} style={{
        height: '107px',
    }}>
      <div className={ROW({alignItems: 'center'})}>
        <AvatarList size={AVATARS.size.small} urls={DEFAULT_SOURCES}/>
        <div className={THREAD_HEADER({marginLeft: '10px'})}>Summer Party</div>
        <Round />
        <TimeAgo date={new Date(new Date().getTime() - 120000)}/>
        <Badge style={{marginLeft: 'auto', marginRight: '15px'}}>+3</Badge>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_HEADER()}>
            My awesome birthday party
          </div>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_TEXT()}>
            Lets go party...
          </div>
      </div>
    </Card>
  ))
  .add('yesterday', () => (
    <Card className={COL()} style={{
        height: '107px',
    }}>
      <div className={ROW({alignItems: 'center'})}>
        <AvatarList size={AVATARS.size.small} urls={DEFAULT_SOURCES}/>
        <div className={THREAD_HEADER({marginLeft: '10px'})}>Summer Party</div>
        <Round />
        <TimeAgo date={new Date(new Date().getTime() - (60000*60*25))}/>
        <Badge style={{marginLeft: 'auto', marginRight: '15px'}}>+3</Badge>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_HEADER()}>
            My awesome birthday party
          </div>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_TEXT()}>
            Lets go party...
          </div>
      </div>
    </Card>
  ))
  .add('2d ago', () => (
    <Card className={COL()} style={{
        height: '107px',
    }}>
      <div className={ROW({alignItems: 'center'})}>
        <AvatarList size={AVATARS.size.small} urls={DEFAULT_SOURCES}/>
        <div className={THREAD_HEADER({marginLeft: '10px'})}>Summer Party</div>
        <Round />
        <TimeAgo date={new Date(new Date().getTime() - (60000*60*25*2))}/>
        <Badge style={{marginLeft: 'auto', marginRight: '15px'}}>+3</Badge>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_HEADER()}>
            My awesome birthday party
          </div>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_TEXT()}>
            Lets go party...
          </div>
      </div>
    </Card>
  ))
  .add('personal', () => (
      <Card className={COL()} style={{
        height: '107px',
      }}>
      <div className={ROW({alignItems: 'center'})}>
        <AvatarList size={AVATARS.size.small} urls={[DEFAULT_SOURCES[0]]}/>
        <div className={THREAD_HEADER({marginLeft: '10px'})}>Mary Jane</div>
        <Round />
        <TimeAgo date={new Date(new Date().getTime() - 120000)}/>
        <Badge style={{marginLeft: 'auto', marginRight: '15px'}}>+4</Badge>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_HEADER()}>
            My awesome birthday party
          </div>
      </div>
      <div className={ROW({height: 'auto'})}>
          <div className={THREAD_TEXT()}>
            Lets go party...
          </div>
      </div>
    </Card>
  ));
