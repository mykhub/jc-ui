import React from 'react';

import { storiesOf } from '@storybook/react';
import { Avatar } from '../src';
import { AVATARS } from '../src/common/constants';

const DEFAULT_SOURCE = "https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg";

storiesOf('Avatar', module)
  .add('small', () => (
    <Avatar source={DEFAULT_SOURCE} />
  ))
  .add('medium', () => (
      <Avatar source={DEFAULT_SOURCE} size={AVATARS.size.medium}/>
  ))
  .add('large', () => (
    <Avatar source={DEFAULT_SOURCE} size={AVATARS.size.large}/>
  ));
