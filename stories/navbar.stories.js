import React from 'react';
import Emojify from 'react-emojione';
import { storiesOf } from '@storybook/react';
import { NavBar, Badge, DropDown, Button } from '../src';
import { FONTS } from '../src/common/constants';
import Icon from '../src/components/Icon/components/Icon';

const EMOJI_SIZE = (hasMargin) => ({
  width: '18px',
  height: '18px',
  marginLeft: hasMargin ? '10px' : '0',
});

storiesOf('Navbar', module)
  .add('default', () => (
    <NavBar>
      <h4>Lorem ipsum</h4>
    </NavBar>
  ))
  .add('with content', () => (
    <div style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row' }}>
      <NavBar>
        <NavBar.Block>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            }}>
            <span style={{
              fontFamily: FONTS.family.default,
              fontSize: FONTS.fontSize.big,
              lineHeight: '24px',
              fontWeight: 'bold',
            }}>Mailbox name</span>
            <div style={{marginLeft: 'auto'}}>
              <DropDown />
            </div>
          </div>
          <div style={{
            isplay: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            fontFamily: FONTS.family.default,
            fontSize: FONTS.fontSize.small,
            lineHeight: '24px',
          }}>humenetskyymaryan@gmail.com</div>
        </NavBar.Block>
        <NavBar.Block>
          <Button><Emojify style={EMOJI_SIZE(false)}>✏️</Emojify>Compose</Button>
        </NavBar.Block>
        <NavBar.Block>
          <NavBar.Item><Emojify style={EMOJI_SIZE(true)}>🔎</Emojify><span style={{marginLeft: '3%'}}>Search</span></NavBar.Item>
          <NavBar.Item><Emojify style={EMOJI_SIZE(true)}>📮</Emojify><span style={{marginLeft: '3%'}}>Inbox</span></NavBar.Item>
          <NavBar.Item><Emojify style={EMOJI_SIZE(true)}>📮</Emojify><span style={{marginLeft: '3%'}}>All Threads</span></NavBar.Item>
          <NavBar.Item><Emojify style={EMOJI_SIZE(true)}>📔</Emojify><span style={{marginLeft: '3%'}}>To-dos</span></NavBar.Item>
        </NavBar.Block>
        <NavBar.Block>
          <DropDown canRotate={true} placeholder='Mailboxes' rightButton={() => (<Icon></Icon>)}>
            <DropDown.Item><Emojify style={EMOJI_SIZE(true)}>👪</Emojify><span style={{marginLeft: '3%'}}>Dream team</span></DropDown.Item>
            <DropDown.Item><Emojify style={EMOJI_SIZE(true)}>👔</Emojify><span style={{marginLeft: '3%'}}>Work</span><div style={{marginLeft: 'auto'}}><Badge styles={{margiLeft: 'auto'}}>+5</Badge></div></DropDown.Item>
            <DropDown.Item><Emojify style={EMOJI_SIZE(true)}>📰</Emojify><span style={{marginLeft: '3%'}}>Newsletters</span><div style={{marginLeft: 'auto'}}><Badge styles={{margiLeft: 'auto'}}>+2</Badge></div></DropDown.Item>
          </DropDown>
        </NavBar.Block>
        <NavBar.Block>
          <DropDown canRotate={true} placeholder='Other'>
            <DropDown.Item><Emojify style={EMOJI_SIZE(true)}>👪</Emojify><span style={{marginLeft: '3%'}}>Dream team</span></DropDown.Item>
            <DropDown.Item><Emojify style={EMOJI_SIZE(true)}>👔</Emojify><span style={{marginLeft: '3%'}}>Work</span><div style={{marginLeft: 'auto'}}><Badge styles={{margiLeft: 'auto'}}>+5</Badge></div></DropDown.Item>
            <DropDown.Item><Emojify style={EMOJI_SIZE(true)}>📰</Emojify><span style={{marginLeft: '3%'}}>Newsletters</span><div style={{marginLeft: 'auto'}}><Badge styles={{margiLeft: 'auto'}}>+2</Badge></div></DropDown.Item>
          </DropDown>
        </NavBar.Block>
      </NavBar>
    </div>
  ));
