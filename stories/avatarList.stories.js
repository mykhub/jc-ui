import React from 'react';

import { storiesOf } from '@storybook/react';
import { AvatarList } from '../src';
import { AVATARS } from '../src/common/constants';

const DEFAULT_SOURCES = [
    'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
];

storiesOf('AvatarList', module)
  .add('small', () => (
    <AvatarList size={AVATARS.size.small} urls={DEFAULT_SOURCES} />
  ))
  .add('medium', () => (
    <AvatarList size={AVATARS.size.medium} urls={DEFAULT_SOURCES} />
  ))
  .add('large', () => (
    <AvatarList size={AVATARS.size.large} urls={DEFAULT_SOURCES} />
  ));
