import React from 'react';

import { storiesOf } from '@storybook/react';
import { Card, AvatarList, Round, TimeAgo, THREAD_TEXT, Badge, FolderName, Button } from '../src';
import { AVATARS } from '../src/common/constants';
import { ROW, COL, THREAD_HEADER } from '../src';   
import Icon from '../src/components/Icon/components/Icon';

const DEFAULT_SOURCES = [
    'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
    'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
];
storiesOf('Threads', module)
  .add('group', () => (
    <div className={COL({
        width: '40vw',
        borderRight: '1px solid #EEF2F6',
        height: '100vh',
        overflow: 'scroll',
        padding: '10px',
        })}>
      <div className={ROW({ 
          alignItems: 'center',
          marginBottom: '30px',
          marginTop: '20px',
          marginLeft: '10px',
        })}>
        <Icon style={{marginRight: '15px'}} icon="arrowLeft"/>
        <Icon icon="arrowRight"/>
      </div>
      <FolderName unreadCount={6} style={{marginLeft: '10px', marginBottom: '25px'}} name="All threads" emoji="📘" />
      <Card className={COL()} style={{
        height: '107px',
        }}>
        <div className={ROW({alignItems: 'center'})}>
          <AvatarList size={AVATARS.size.small} urls={DEFAULT_SOURCES}/>
          <div className={THREAD_HEADER({marginLeft: '10px'})}>Summer Party</div>
          <Round />
          <TimeAgo date={new Date(new Date().getTime() - 120000)}/>
          <Badge style={{marginLeft: 'auto', marginRight: '15px'}}>+3</Badge>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_HEADER()}>
              My awesome birthday party
            </div>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_TEXT()}>
              Lets go party...
            </div>
        </div>
      </Card>
      <Card className={COL()} style={{
        height: '107px',
        }}>
        <div className={ROW({alignItems: 'center'})}>
          <AvatarList size={AVATARS.size.small} urls={DEFAULT_SOURCES}/>
          <div className={THREAD_HEADER({marginLeft: '10px'})}>Summer Party</div>
          <Round />
          <TimeAgo date={new Date(new Date().getTime() - (60000*60*25))}/>
          <Badge style={{marginLeft: 'auto', marginRight: '15px'}}>+3</Badge>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_HEADER()}>
              My awesome birthday party
            </div>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_TEXT()}>
              Lets go party...
            </div>
        </div>
      </Card>
      <Card className={COL()} style={{
          height: '107px',
        }}>
        <div className={ROW({alignItems: 'center'})}>
          <AvatarList size={AVATARS.size.small} urls={[DEFAULT_SOURCES[0]]}/>
          <div className={THREAD_HEADER({marginLeft: '10px'})}>Mary Jane</div>
          <Round />
          <TimeAgo date={new Date(new Date().getTime())}/>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_HEADER()}>
              My awesome birthday party
            </div>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_TEXT()}>
              Lets go party...
            </div>
        </div>
      </Card>
      <Card className={COL()} style={{
          height: '107px',
        }}>
        <div className={ROW({alignItems: 'center'})}>
          <AvatarList size={AVATARS.size.small} urls={[DEFAULT_SOURCES[0]]}/>
          <div className={THREAD_HEADER({marginLeft: '10px'})}>Mary Jane</div>
          <Round />
          <TimeAgo date={new Date(new Date().getTime() - 60000 * 60 * 24 * 7)}/>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_HEADER()}>
              My awesome birthday party
            </div>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_TEXT()}>
              Lets go party...
            </div>
        </div>
      </Card>
      <Card className={COL()} style={{
          height: '107px',
        }}>
        <div className={ROW({alignItems: 'center'})}>
          <AvatarList size={AVATARS.size.small} urls={[DEFAULT_SOURCES[0]]}/>
          <div className={THREAD_HEADER({marginLeft: '10px'})}>Mary Jane</div>
          <Round />
          <TimeAgo date={new Date(new Date().getTime())}/>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_HEADER()}>
              My awesome birthday party
            </div>
        </div>
        <div className={ROW({height: 'auto'})}>
            <div className={THREAD_TEXT()}>
              Lets go party...
            </div>
        </div>
      </Card>
    </div>
  ));
