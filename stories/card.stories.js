import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Card, AvatarList } from '../src';

const DEFAULT_SOURCES = [
  'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
  'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
  'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
  'https://image.shutterstock.com/image-vector/blank-avatar-photo-place-holder-260nw-1095249842.jpg', 
  'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
  'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png',
];

storiesOf('Card', module)
  .add('default', () => (
    <Card action={action('card clicked')}>Corporate party</Card>
  ))
  .add('with photos', () => (
    <Card><AvatarList urls={DEFAULT_SOURCES}></AvatarList></Card>
  ))
  .add('list of cards', () => (
    <div>
    {[1,2,3,4,5].map(i => (<Card 
      onClick={action('card selected')}
    >Paul Braun</Card>))
    }</div>
  ));

