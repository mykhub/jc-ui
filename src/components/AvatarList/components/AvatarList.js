import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { Avatar } from '../../..';
import { AVATAR_LIST } from '../styles/avatarList';
import { AVATARS } from '../../../common/constants';

const AvatarList = ({ urls, size, style }) => (
  <div className={cx(
    'jc-avatar-list',
    AVATAR_LIST(style),
  )}>
    {
        urls.slice(0, 5).map((url, index) => (
          <Avatar
            style={{
              marginLeft: index === 0 ? '0' : `calc(-${size}*0.7)`,
              borderWidth: '2px',
              borderColor: 'inherit',
              borderStyle: 'solid',
              position: 'relative',
              zIndex: 10 - index,
            }}
            size={size}
            key={url}
            source={url} />
        ))
      }
  </div>
);

AvatarList.defaultProps = {
  urls: [],
  size: AVATARS.size.small,
  style: {},
};

AvatarList.propTypes = {
  urls: PropTypes.arrayOf(PropTypes.string),
  size: PropTypes.string,
  style: PropTypes.instanceOf(Object),
};

export default AvatarList;
