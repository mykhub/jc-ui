import cxs from 'cxs';

export const AVATAR_LIST = style => cxs({
  height: 'auto',
  width: 'auto',
  borderColor: 'inherit',
  ...style,
});
