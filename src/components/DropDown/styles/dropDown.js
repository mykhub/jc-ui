import cxs from 'cxs';
import { FONTS } from '../../../common/constants';

export const DROPDOWN = styles => cxs({
  display: 'flex',
  flexDirection: 'column',
  width: 'auto',
  height: 'auto',
  backgroundColor: 'inherit',
  fontFamily: FONTS.family.default,
  fontColor: FONTS.color.default,
  ...styles,
});

export const PLACEHOLDER = styles => cxs({
  right: '0',
  fontWeight: 'bold',
  fontSize: FONTS.fontSize.default,
  fontFamily: FONTS.family.default,
  lineHeight: '19px',
  marginLeft: '3%',
  ...styles,
});
