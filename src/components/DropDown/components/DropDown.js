import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Item from './Item';
import Icon from '../../Icon/components/Icon';

import { DROPDOWN, PLACEHOLDER } from '../styles/dropDown';

class DropDown extends PureComponent {
  static Item = Item;

  state = {
    open: false,
  };

  handleClick = () => {
    this.setState(prevState => ({ open: !prevState.open }));
  };

  render() {
    const {
      children,
      styles,
      canRotate,
      placeholder,
      placeholderStyles,
      rightButton,
    } = this.props;
    const { open } = this.state;

    return (
      <div>
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Icon
            styles={{
              backgroundColor: 'inherit',
              width: '20px',
              height: '20px',
              marginLeft: '10px',
              transform: canRotate ? `rotate(${270 + 90 * open}deg)` : 'none',
            }}
            icon="arrowDown"
            onClick={this.handleClick} />
          <span tabIndex={0} role="button" onClick={this.handleClick} onKeyDown={this.handleClick} className={cx('jc-dropdown-icon-container', PLACEHOLDER(placeholderStyles))}>
            { placeholder }
          </span>
          { rightButton }
        </div>
        <div className={cx(
          'jc-dropdown',
          DROPDOWN(styles),
        )}>
          { open && children }
        </div>
      </div>
    );
  }
}

DropDown.defaultProps = {
  styles: {},
  children: null,
  canRotate: false,
  placeholder: null,
  placeholderStyles: {},
  rightButton: null,
};

DropDown.propTypes = {
  children: PropTypes.node,
  styles: PropTypes.instanceOf(Object),
  canRotate: PropTypes.bool,
  placeholder: PropTypes.string,
  placeholderStyles: PropTypes.instanceOf(Object),
  rightButton: PropTypes.node,
};

export default DropDown;
