import cxs from 'cxs';

import { FONTS, COLORS } from '../../../common/constants';

export const BUTTON = cxs({
  fontStyle: FONTS.family.default,
  fontFamily: FONTS.family.default,
  width: 'auto',
  maxWidth: '210px',
  height: '35px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  background: COLORS.white,
  border: `1px solid ${COLORS.gray.athensGray}`,
  boxSizing: 'border-box',
  boxShadow: '0px 3px 2px rgba(0, 0, 0, 0.1)',
  borderRadius: '10px',
  cursor: 'pointer',
});
