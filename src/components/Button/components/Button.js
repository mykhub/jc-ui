import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { BUTTON } from '../styles/button';

const Button = ({ children }) => (
  <div className={cx('jc-button', BUTTON)} type="button">
    { children }
  </div>
);

Button.propTypes = {
  children: PropTypes.node,
};

Button.defaultProps = {
  children: null,
};

export default Button;
