import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { BADGE } from '../styles/badge';
import { COLORS } from '../../../common/constants';

const Badge = ({ children, color, style }) => (
  <span className={cx('jc-badge', BADGE(style, color))} type="button">
    { children }
  </span>
);

Badge.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  style: PropTypes.instanceOf(Object),
};

Badge.defaultProps = {
  children: null,
  color: COLORS.blue.royalBlue,
  style: {},
};

export default Badge;
