import cxs from 'cxs';

import { FONTS, COLORS } from '../../../common/constants';

export const BADGE = (style, color) => cxs({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  width: '27px',
  height: '20px',
  background: color,
  borderRadius: '6px',
  fontStyle: 'normal',
  fontFamily: FONTS.family.default,
  fontWeight: 'bold',
  fontSize: '10px',
  lineHeight: '12px',
  color: COLORS.white,
  ...style,
});
