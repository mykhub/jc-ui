import cxs from 'cxs';
import { FONTS, COLORS } from '../../../common/constants';

export const CARD = style => cxs({
  width: 'auto',
  height: 'auto',
  borderRadius: '10px',
  backgroundColor: COLORS.white,
  paddingTop: '17px',
  paddingLeft: '14px',
  cursor: 'pointer',
  fontFamily: FONTS.family.default,
  сolor: FONTS.color.outerSpace,
  fontSize: FONTS.fontSize.default,
  borderColor: COLORS.white,
  ...style,

  '.jc-card:hover, .jc-card:focus, .jc-card:active': {
    backgroundColor: COLORS.gray.athensGray,
    borderColor: COLORS.gray.athensGray,
  },
});
