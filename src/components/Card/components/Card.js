import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { CARD } from '../styles/card';

const Card = ({ children, style, onClick }) => (
  <div
    onClick={onClick}
    onKeyDown={onClick}
    tabIndex={0}
    role="button"
    className={cx('jc-card', CARD(style))}>
    { children }
  </div>
);

Card.defaultProps = {
  style: {},
  children: null,
};

Card.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.instanceOf(Object),
};

export default Card;
