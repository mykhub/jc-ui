import cxs from 'cxs';

import { FONTS } from '../../../common/constants';

export const BLOCK = styles => cxs({
  display: 'flex',
  flexDirection: 'column',
  backgroundColor: 'inherit',
  marginBottom: '30px',
  fontFamily: FONTS.family.default,
  fontColor: FONTS.color.shuttleGray,
  fontSize: FONTS.fontSize.default,
  ...styles,
});
