import cxs from 'cxs';

import { FONTS, COLORS } from '../../../common/constants';

export const ITEM = styles => cxs({
  display: 'flex',
  borderRadius: '5px',
  height: '36px',
  flexDirection: 'row',
  alignItems: 'center',
  backgroundColor: 'inherit',
  fontFamily: FONTS.family.default,
  fontColor: FONTS.color.shuttleGray,
  fontSize: FONTS.fontSize.default,
  '.jc-item:hover, .jc-item:focus, .jc-item:active': {
    backgroundColor: COLORS.gray.mystic,
    borderColor: COLORS.gray.mystic,
  },
  ...styles,
});
