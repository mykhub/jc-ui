import cxs from 'cxs';

import { COLORS } from '../../../common/constants';

export const NAVBAR_CONTAINER = cxs({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  height: '100vh',
  width: '260px',
  top: 0,
  left: 0,
  overflowX: 'hidden',
  padding: '15px',
  fontFamily: '',
  border: `1px solid ${COLORS.gray.athensGray}`,
  background: `linear-gradient(180deg, ${COLORS.blue.catskillWhite} 0%, ${COLORS.pink.prim} 100%);`,
  boxSizing: 'border-box',
});
