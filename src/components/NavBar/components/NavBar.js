import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { NAVBAR_CONTAINER } from '../styles/navbar';
import Item from './Item';
import Block from './Block';

class NavBar extends Component {
  static Item = Item;

  static Block = Block;

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { children } = this.props;

    return (
      <div className={cx('jc-navbar-container', NAVBAR_CONTAINER)}>
        {children}
      </div>
    );
  }
}

NavBar.propTypes = {
  children: PropTypes.node,
};

NavBar.defaultProps = {
  children: null,
};

export default NavBar;
