import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { BLOCK } from '../styles/block';

const Block = ({
  children,
  style,
}) => (
  <div
    tabIndex={0}
    role="button"
    className={cx('jc-block', BLOCK(style))}>
    { children }
  </div>
);

Block.defaultProps = {
  style: {},
  children: null,
};

Block.propTypes = {
  children: PropTypes.node,
  style: PropTypes.instanceOf(Object),
};

export default Block;
