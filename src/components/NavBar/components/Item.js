import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { ITEM } from '../styles/item';

const Item = ({
  children,
  style,
  onClick,
}) => (
  <div
    onClick={onClick}
    tabIndex={0}
    role="button"
    onKeyDown={onClick}
    className={cx('jc-item', ITEM(style))}>
    { children }
  </div>
);

Item.defaultProps = {
  style: {},
  children: null,
  onClick: null,
};

Item.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  style: PropTypes.instanceOf(Object),
};

export default Item;
