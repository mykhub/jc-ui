import React, { Component } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { TIME_AGO } from '../styles/timeAgo';

class TimeAgo extends Component {
  minuteMiliseconds = 60 * 1000;

  hourMiliseconds = this.minuteMiliseconds * 60;

  dayMiliseconds = this.hourMiliseconds * 24;

  constructor(props) {
    super(props);
    this.state = {
      next: null,
      value: null,
    }
  }

  componentDidMount() {
    const { date, formatDate } = this.props;

    this.setState(formatDate ? formatDate(date, new Date()) : this.formatDate(date, new Date()));

    this.timerId = setInterval(
      () => this.updateValue(),
      60000,
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  updateValue = () => {
    const { next } = this.state;
    const { date, formatDate } = this.props;
    const now = new Date();
    if (next - now <= 0) {
      this.setState(formatDate ? formatDate(date, now) : this.formatDate(date, now));
    }
  }

  formatDate = (date, now) => {
    const d = now - date;
    const {
      minutesLabel,
      hoursLabel,
      daysLabel,
      yesterdayLabel,
    } = this.props;
    if (d < this.minuteMiliseconds * 2) {
      return {
        value: 'now',
        next: new Date(date.getTime() + (this.minuteMiliseconds * 2 - d)),
      };
    } if (d < this.hourMiliseconds) {
      const minutes = Math.floor(d / this.minuteMiliseconds);
      return {
        value: this.formatString(minutesLabel, minutes),
        next: new Date(date.getTime() + (this.minuteMiliseconds - (d % 60))),
      };
    } if (d < this.dayMiliseconds) {
      const hours = Math.floor(d / this.hourMiliseconds);
      return {
        value: this.formatString(hoursLabel, hours),
        next: new Date(date.getTime() + (this.hourMiliseconds - (d % this.hourMiliseconds))),
      };
    }
    const days = Math.floor(d / this.dayMiliseconds);
    return {
      value: days === 1 ? yesterdayLabel : this.formatString(daysLabel, days),
      next: new Date(date.getTime() + (this.dayMiliseconds - (d % this.dayMiliseconds))),
    };
  }

  formatString = (pattern, ...arg) => {
    let s = pattern;
    for (let i = 0; i < arg.length; i += 1) {
      const reg = new RegExp(`\\{${i}\\}`, 'gm');
      s = s.replace(reg, arg[i]);
    }
    return s;
  }

  render() {
    const { value } = this.state;
    const { style } = this.props;
    return (
      <div className={cx('jc-time-ago', TIME_AGO(style))}>{ value }</div>
    );
  }
}

TimeAgo.defaultProps = {
  date: null,
  style: {},
  minutesLabel: '{0}m ago',
  hoursLabel: '{0}h ago',
  daysLabel: '{0}d ago',
  yesterdayLabel: 'yesterday',
  formatDate: null,
};

TimeAgo.propTypes = {
  date: PropTypes.instanceOf(Date),
  style: PropTypes.instanceOf(Object),
  minutesLabel: PropTypes.string,
  hoursLabel: PropTypes.string,
  daysLabel: PropTypes.string,
  yesterdayLabel: PropTypes.string,
  formatDate: PropTypes.func,
};

export default TimeAgo;
