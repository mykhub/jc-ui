import cxs from 'cxs';

import { FONTS } from '../../../common/constants';

export const TIME_AGO = style => cxs({
  fontColor: FONTS.color.slate,
  fontFamily: FONTS.family.default,
  fontSize: FONTS.fontSize.small,
  ...style,
});
