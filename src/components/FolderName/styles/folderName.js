import cxs from 'cxs';

import { FONTS } from '../../../common/constants/fonts';

export const FOLDER_NAME = () => cxs({
  fontSize: FONTS.fontSize.folderName,
  fontFamily: FONTS.family.default,
  fontColor: FONTS.color.default,
  fontWeight: 'bold',
  alignItems: 'center',
});

export const FOLDER_EMOJI = () => cxs({
  width: '26px',
  height: '26px',
});

export const FOLDER_EMOJI_CONTAINER = () => cxs({
  marginRight: '12px',
});

export const FOLDER_UNREAD_LABEL = () => cxs({
  fontFamily: FONTS.family.default,
  fontSize: FONTS.fontSize.default,
  fontColor: FONTS.color.slate,
});
