import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Emojify from 'react-emojione';

import {
  FOLDER_NAME,
  FOLDER_EMOJI,
  FOLDER_EMOJI_CONTAINER,
  FOLDER_UNREAD_LABEL,
} from '../styles/folderName';
import { ROW, COL } from '../../../common/styles';

const FolderName = ({
  style,
  emoji,
  name,
  unreadCount,
  unreadLocalization,
}) => (
  <div className={COL(style)}>
    <div className={cx('jc-folder-name', FOLDER_NAME(), ROW())}>
      <div className={cx('jc-folder-emoji-container', FOLDER_EMOJI_CONTAINER())}>
        <Emojify className={cx('jc-folder-emoji', FOLDER_EMOJI())}>{emoji}</Emojify>
      </div>
      {name}
    </div>
    <div className={cx('jc-folder-unread', FOLDER_UNREAD_LABEL(), ROW({ marginTop: '5px' }))}>
      {unreadLocalization(unreadCount)}
    </div>
  </div>
);

FolderName.propTypes = {
  style: PropTypes.instanceOf(Object),
  emoji: PropTypes.string,
  name: PropTypes.string,
  unreadCount: PropTypes.number,
  unreadLocalization: PropTypes.func,
};

FolderName.defaultProps = {
  style: {},
  emoji: '',
  name: '',
  unreadCount: 0,
  unreadLocalization: (number) => {
    if (number === 0) {
      return 0;
    }
    if (number === 1) {
      return `${number} new email`;
    }
    return `${number} new emails`;
  },
};

export default FolderName;
