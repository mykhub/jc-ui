import cxs from 'cxs';

import { COLORS } from '../../../common/constants';

export const AVATAR = (size, style) => cxs({
  borderRadius: '50%',
  backgroundColor: COLORS.blue,
  width: size,
  height: size,
  ...style,
});
