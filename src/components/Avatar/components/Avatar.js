import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { AVATAR } from '../styles/avatar';
import { AVATARS } from '../../../common/constants';

const Avatar = ({ source, size, style }) => (
  <img
    src={source}
    className={cx(
      'jc-avatar',
      AVATAR(size, style),
    )}
    alt="AVATAR" />
);

Avatar.defaultProps = {
  source: null,
  size: AVATARS.size.small,
  style: {},
};

Avatar.propTypes = {
  source: PropTypes.string,
  size: PropTypes.string,
  style: PropTypes.instanceOf(Object),
};

export default Avatar;
