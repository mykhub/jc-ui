import cxs from 'cxs';

import { COLORS } from '../../../common/constants';

export const ROUND = style => cxs({
  fontColor: COLORS.gray.slateGray,
  width: '4px',
  height: '4px',
  marginLeft: '7px',
  marginRight: '7px',
  ...style,
});
