import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import { ROUND } from '../styles/round';
import { ICONS } from '../../../common/constants';

const Round = ({ style }) => (
  <img
    src={ICONS.round.source}
    alt={ICONS.round.alt}
    className={cx('jc-round', ROUND(style))} />
);

Round.defaultProps = {
  style: null,
};

Round.propTypes = {
  style: PropTypes.instanceOf(Object),
};

export default Round;
