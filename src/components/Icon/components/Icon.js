import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { ICONS } from '../../../common/constants';
import { ICON } from '../styles/icon';

const Icon = ({ icon, style, onClick }) => (
  <div
    role="button"
    tabIndex={0}
    onClick={onClick}
    onKeyDown={onClick}
    className={cx('jc-icon', ICON(style))}>
    <img
      src={ICONS[icon].source}
      alt={ICONS[icon].alt} />
  </div>
);

Icon.defaultProps = {
  style: {},
  onClick: null,
};

Icon.propTypes = {
  icon: PropTypes.string.isRequired,
  style: PropTypes.instanceOf(Object),
  onClick: PropTypes.func,
};

export default Icon;
