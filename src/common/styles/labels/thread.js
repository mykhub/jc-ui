import cxs from 'cxs';
import cx from 'classnames';

import { FONTS } from '../../constants';

export const THREAD_HEADER = style => cx('jc-thread-header', cxs({
  fontFamily: FONTS.family.default,
  fontStyle: 'normal',
  fontWeight: 'bold',
  fontColor: FONTS.color.default,
  fontSize: FONTS.fontSize.default,
  ...style,
}));

export const THREAD_TEXT = style => cx('jc-thread-text', cxs({
  fontFamily: FONTS.family.default,
  fontStyle: 'normal',
  fontWeight: 'normal',
  fontColor: FONTS.color.charade,
  fontSize: FONTS.fontSize.default,
  ...style,
}));
