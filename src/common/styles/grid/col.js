import cxs from 'cxs';
import cx from 'classnames';

export const COL = style => cx('jc-col', cxs({
  display: 'flex',
  flexDirection: 'column',
  ...style,
}));
