import cx from 'classnames';
import cxs from 'cxs';

export const ROW = styles => cx('jc-row', cxs({
  display: 'flex',
  flexDirection: 'row',
  borderColor: 'inherit',
  minHeight: '27px',
  width: '100%',
  position: 'relative',
  ...styles,
}));
