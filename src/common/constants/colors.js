export const COLORS = {
  white: '#FFFFFF',
  gray: {
    athensGray: '#F7F8FA',
    shuttleGray: '#686A76',
    slateGray: '#798794',
    outerSpace: '#2f3439',
    mystic: '#E6EBF0',
    charade: '#2E333D',
    trout: '#515A66',
  },
  blue: {
    catskillWhite: '#F5F8FA',
    royalBlue: '#4F6BEB',
  },
  pink: {
    prim: '#FBF7FA',
  },
};

export default COLORS;

