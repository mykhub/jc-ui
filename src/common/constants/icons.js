import plus from '../assets/icons/plus.svg';
import arrowDown from '../assets/icons/arrowDown.svg';
import round from '../assets/icons/round.svg';
import arrowRight from '../assets/icons/arrowRight.svg';
import arrowLeft from '../assets/icons/arrowLeft.svg';

export const ICONS = {
  plus: { source: plus, alt: 'plus' },
  arrowDown: { source: arrowDown, alt: 'arrow down' },
  arrowLeft: { source: arrowLeft, alt: 'arrow left' },
  arrowRight: { source: arrowRight, alt: 'arrow right' },
  round: { source: round, alt: 'round' },
};
