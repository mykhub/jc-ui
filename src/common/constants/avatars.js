export const AVATARS = {
  size: {
    small: '26px',
    medium: '30px',
    large: '46px',
  },
};
