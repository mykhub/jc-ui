import { COLORS } from './colors';

export const FONTS = {
  family: {
    default: 'Roboto',
  },
  color: {
    default: COLORS.gray.outerSpace,
    charade: COLORS.gray.charade,
    slate: COLORS.gray.slateGray,
  },
  fontSize: {
    default: '14px',
    small: '12px',
    folderName: '24px',
  },
};
