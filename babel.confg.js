module.exports = {
  plugins: [
    '@babel/plugin-transform-flow-strip-types',
    '@babel/plugin-syntax-dynamic-import',
    'transform-class-properties',
    'babel-plugin-transform-dynamic-import',
    [
      '@babel/plugin-transform-runtime',
      { regenerator: true },
    ],
  ],
  presets: [
    [
      '@babel/preset-env', { targets: { node: 'current' } },
      '@babel/preset-react',
      'react',
    ],
  ],
  env: {
    production: {
      presets: ['minify'],
    },
  },
};
