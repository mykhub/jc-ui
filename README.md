### **How to publish**
##### There is only two branches for publishing:
branch | version | example
--- | --- | ---
`develop` | patch | `1.0.x` 
`master`  | minor | `1.x.0`

##### Git push option for ci-skip
```git push -o ci.skip```
